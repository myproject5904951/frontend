import { ref } from 'vue'
import { defineStore } from 'pinia'
import houseService from '@/service/house'
import type House from '@/types/House'
// import { useLoadingStore } from './loading'

export const useHouseStore = defineStore('house', () => {
  const houses = ref<House[]>([])
  // const loadingStore = useLoadingStore();

  async function getHouses() {
    // loadingStore.isLoading = true;
    try {
      const res = await houseService.getHouses()
      houses.value = res.data
    } catch (e) {
      console.log(e)
    }
    // loadingStore.isLoading = false;
  }

  return {
    getHouses,
    houses
  }
})
