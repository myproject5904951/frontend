import { ref } from 'vue'
import { defineStore } from 'pinia'
import type Receipt from '@/types/Receipt'
import receiptService from '@/service/receipt'
import type Invoice from '@/types/Invoice'
import invoiceService from '@/service/invoice'
import residentService from '@/service/resident'
import jurusticService from '@/service/juristic'
import logService from '@/service/log'
import { useLoadingStore } from './loading'
import type Log from '@/types/Log'

export const useReceiptStore = defineStore('receipt', () => {
  const receipts = ref<Receipt[]>([])
  const selectReceipt = ref<Receipt>()
  const selectPackate = ref()
  const payDialog = ref(false)
  const confirmReceiptDialog = ref(false)
  const confirmReturnDialog = ref(false)
  const billImg = ref('')
  const loadingStore = useLoadingStore()
  const totalCMF = ref(0)
  async function getReceipts() {
    loadingStore.isLoading = true
    try {
      totalCMF.value = 0
      const res = await receiptService.getReceipts()
      receipts.value = res.data
      for (const i of receipts.value) {
        if(i.juristic){
          totalCMF.value += parseFloat(i.paid)
        }
      }
      receipts.value.sort(function (a, b) {
        const keyA = new Date(a.date);
        const keyB = new Date(b.date);
        // Compare the 2 dates
        if (keyA > keyB) return -1;
        if (keyA < keyB) return 1;
        return 0;
      })
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function createReceipt(receipt: Receipt, lstInv: Invoice[]) {
    loadingStore.isLoading = true
    try {
      if (receipt.payment_method == 'Mobile Banking') {
        receipt.billImg = billImg.value
      }
      receipt.resident = (await residentService.getResidentByID(receipt.residentId!)).data
      const juristicId = localStorage.getItem('juristicId')
      // console.log(juristicId)
      if(juristicId){
        receipt.juristic = (await jurusticService.getJuristicByID(juristicId)).data
      }
      const res = await receiptService.saveReceipt(receipt)
      selectReceipt.value = res.data
      // console.log(selectReceipt.value?.resident)
      for (const inv of lstInv) {
        const updInv = ref<Invoice>()
        const oldInv = await invoiceService.getInvoiceByID(inv.id!)
        updInv.value = oldInv.data
        updInv.value!.receipt = selectReceipt.value
        // console.log(selectReceipt.value?.resident)
        await invoiceService.updateInvoice(updInv.value!.id!, updInv.value!)
      }
      const activity = ref('')
      const role = localStorage.getItem('role')
      if (role === '3') {
        const residentId = localStorage.getItem('residentId')
        activity.value = `ลูกบ้านจ่ายเงินค่าส่วนกลาง receipt_id: ${selectReceipt.value!.id}`
        const log = ref<Log>({
          activity: activity.value,
          residentId: parseInt(residentId!)
        })
        const res = await logService.saveLog(log.value)
        // console.log(res)
      } else {
        const juristicId = localStorage.getItem('juristicId')
        activity.value = `พนักงานจ่ายเงินค่าส่วนกลาง receipt_id: ${selectReceipt.value!.id}`
        const log = ref<Log>({
          activity: activity.value,
          juristicId: parseInt(juristicId!)
        })
        const res = await logService.saveLog(log.value)
        // console.log(res)
      }
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
    payDialog.value = true
  }

  const showConfirmDialog = (receipt: Receipt) => {
    selectReceipt.value = receipt
    confirmReceiptDialog.value = true
  }

  const showConfirmReturnDialog = (receipt: Receipt) => {
    selectReceipt.value = receipt
    confirmReturnDialog.value = true
  }

  const confirm = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref(`ยืนยันใบเสร็จ id: ${selectReceipt.value?.id!}`)
      const juristicId = localStorage.getItem('juristicId')
      const juristic = await jurusticService.getJuristicByID(
        parseInt(localStorage.getItem('juristicId')!)
      )
      selectReceipt.value!.juristic = juristic.data
      const res = await receiptService.updateReceipt(selectReceipt.value?.id!, selectReceipt.value!)
      // console.log(res)
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      await logService.saveLog(log.value)
      confirmReceiptDialog.value = false
    } catch (e) {
      console.log(e)
    }
    await getReceipts()
    loadingStore.isLoading = false
  }

  const deleteReceipt = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref(`ลบใบเสร็จ id: ${selectReceipt.value?.id!}`)
      const juristicId = localStorage.getItem('juristicId')
      const res = await receiptService.deleteReceipt(selectReceipt.value?.id!)
      // console.log(res)
      confirmReturnDialog.value = false
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      await logService.saveLog(log.value)
    } catch (e) {
      console.log(e)
    }
    await getReceipts()
    loadingStore.isLoading = false
  }

  return {
    getReceipts,
    createReceipt,
    receipts,
    selectReceipt,
    selectPackate,
    payDialog,
    billImg,
    confirmReceiptDialog,
    showConfirmDialog,
    confirm,
    confirmReturnDialog,
    showConfirmReturnDialog,
    deleteReceipt,
    totalCMF
  }
})
