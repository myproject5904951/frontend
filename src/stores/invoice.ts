import { ref } from 'vue'
import { defineStore } from 'pinia'
import type Invoice from '@/types/Invoice'
import invoiceService from '@/service/invoice'
import logService from '@/service/log'
import { useLoadingStore } from './loading'
import type Log from '@/types/Log'

export const useInvoiceStore = defineStore('invoice', () => {
  const invoice = ref<Invoice[]>([])
  const selectInvoice = ref<Invoice>()
  const cancelDialog = ref(false)
  const createInvDialog = ref(false)
  const loadingStore = useLoadingStore()

  async function getInvoices() {
    loadingStore.isLoading = true
    try {
      const res = await invoiceService.getInvoices()
      invoice.value = res.data
      // console.log(invoice.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function getInvoiceByID(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await invoiceService.getInvoiceByID(id)
      selectInvoice.value = res.data
      // console.log(selectInvoice.value?.resident?.house)
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const editedInvoice = ref<Invoice>({
    house_number: '',
    amount: 0.0,
    receipt: null,
    year: new Date().getFullYear() + 543
  })

  const saveInvoice = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref('')
      const juristicId = localStorage.getItem('juristicId')
      if (editedInvoice.value.id) {
        //edt id not empty = edt
        const res = await invoiceService.updateInvoice(editedInvoice.value.id, editedInvoice.value)
        // console.log(res)
      } else {
        //new
        activity.value = `เพิ่มใบแจ้งหนี้ประจำปี พ.ศ.${editedInvoice.value.year}`
        const log = ref<Log>({
          activity: activity.value,
          juristicId: parseInt(juristicId!)
        })
        const resLog = await logService.saveLog(log.value)
        editedInvoice.value.year! -= 543
        const res = await invoiceService.saveInvoice(editedInvoice.value)
      }
    } catch (e) {
      console.log(e)
    }
    await getInvoices()
    clear()
    loadingStore.isLoading = false
  }

  const clear = () => {
    cancelDialog.value = false
    createInvDialog.value = false
    editedInvoice.value = {
      house_number: '',
      amount: 0.0,
      receipt: null,
      year: new Date().getFullYear() + 543
    }
  }

  const selectedInvoice = async (id: number) => {
    const res = await invoiceService.getInvoiceByID(id)
    selectInvoice.value = res.data
    console.log(selectInvoice.value)
  }

  return {
    invoice,
    getInvoices,
    editedInvoice,
    saveInvoice,
    selectedInvoice,
    selectInvoice,
    getInvoiceByID,
    cancelDialog,
    clear,
    createInvDialog
  }
})
