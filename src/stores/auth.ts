import { defineStore } from 'pinia'
import auth from '@/service/auth'
import router from '@/router'
import { ref } from 'vue'
import userService from '@/service/user'
import { useLoadingStore } from './loading'
import Log from '@/types/Log'
import logService from '@/service/log'
import { useMessageStore } from "./message";

export const useAuthStore = defineStore('auth', () => {
  const id = ref()
  const username = ref()
  const role = ref()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore();

  const isLogin = () => {
    const user = localStorage.getItem('user')
    if (user) {
      id.value = localStorage.getItem('id')
      username.value = localStorage.getItem('username')
      role.value = localStorage.getItem('role')
      if (role.value) return true
    }
    return false
  }

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true
    try {
      const activity = ref('')
      const juristicId = ref('')
      const residentId = ref('')
      const res = await auth.login(username, password)
      // console.log(res)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('token', res.data.access_token)
      localStorage.setItem('id', res.data.user.id)
      localStorage.setItem('username', res.data.user.username)
      localStorage.setItem('role', res.data.user.role)
      const user = await userService.getUserByID(res.data.user.id)
      if (user.data.juristic !== null) {
        localStorage.setItem('juristicId', user.data.juristic.id)
        localStorage.setItem('name', user.data.juristic.name)
        juristicId.value = localStorage.getItem('juristicId')
        activity.value = `พนักงาน id: ${juristicId.value} เข้าสู่ระบบ`
        const log = ref<Log>({
          activity: activity.value,
          juristicId: parseInt(juristicId.value)
        })
        await logService.saveLog(log.value)
      } else if (user.data.resident !== null) {
        localStorage.setItem('residentId', user.data.resident.id)
        localStorage.setItem('name', user.data.resident.name)
        residentId.value = localStorage.getItem('residentId')
        activity.value = `ลูกบ้าน id: ${residentId.value} เข้าสู่ระบบ`
        const log = ref<Log>({
          activity: activity.value,
          residentId: parseInt(residentId.value)
        })
        await logService.saveLog(log.value)
      }
      const role = localStorage.getItem('role')
      if (role !== '3') {
        router.replace('/')
      }
      else {
        const residentId = localStorage.getItem('residentId')
        router.replace('/resident/' + residentId)
      }

    } catch (e) {
      messageStore.showMessage("กรุณาตรวจสอบข้อมูล Username และ Password อีกครั้ง");
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const logout = async () => {
    loadingStore.isLoading = true
    const activity = ref('')
    const juristicId = ref('')
    const residentId = ref('')
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    localStorage.removeItem('username')
    localStorage.removeItem('role')
    localStorage.removeItem('id')
    if (localStorage.getItem('juristicId') !== null) {
      juristicId.value = localStorage.getItem('juristicId')
      activity.value = `พนักงาน id: ${juristicId.value} ออกจากระบบ`
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId.value)
      })
      await logService.saveLog(log.value)
      localStorage.removeItem('juristicId')

    } else if (localStorage.getItem('residentId') !== null) {
      residentId.value = localStorage.getItem('residentId')
      activity.value = `ลูกบ้าน id: ${residentId.value} ออกจากระบบ`
      const log = ref<Log>({
        activity: activity.value,
        residentId: parseInt(residentId.value)
      })
      await logService.saveLog(log.value)
      localStorage.removeItem('residentId')

    }
    localStorage.removeItem('name')
    username.value = ''
    role.value = ''
    id.value = ''
    // console.log(localStorage.getItem('id'))
    router.replace('/login')
    loadingStore.isLoading = false
  }

  return { isLogin, login, logout, username, role, id }
})
