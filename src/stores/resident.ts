import { ref } from 'vue'
import { defineStore } from 'pinia'
import type Resident from '@/types/Resident'
import residentService from '@/service/resident'
import logService from '@/service/log'
import type House from '@/types/House'
import { useLoadingStore } from './loading'
import type Log from '@/types/Log'

export const useResidentStore = defineStore('resident', () => {
  const residents = ref<Resident[]>([])
  const createDialog = ref(false)
  const editDialog = ref(false)
  const deleteDialog = ref(false)
  const deleteId = ref(0)
  const selectResident = ref<Resident>()
  const loadingStore = useLoadingStore()
  const addHouseNumber = ref()

  async function getResidents() {
    loadingStore.isLoading = true
    try {
      const res = await residentService.getResidents()
      residents.value = res.data
      for (const i of residents.value) {
        const hn = []
        for (const j in i.house) {
          hn.push(i.house![parseInt(j)].house_number)
        }
        i.house_number = hn
      }
      // console.log(residents.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function getResidentByID(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await residentService.getResidentByID(id)
      selectResident.value = res.data
      // console.log(selectResident.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const house_number = ref<string[]>([])
  const house = ref<House[]>([])

  async function getHouseNumberByResidentID(id: number) {
    loadingStore.isLoading = true
    try {
      house_number.value = []
      house.value = []
      const res = await residentService.getHouseNumberByResidentID(id)
      house.value = res.data.house
      // console.log(house.value.length)
      for (const item of house.value) {
        house_number.value.push(item.house_number)
      }
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const editedResident = ref<Resident>({
    name: '',
    phone: '',
    email: '',
    house_number: '',
  })

  const deleteResident = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref(`ลบข้อมูลลูกบ้าน id: ${deleteId.value}`)
      const juristicId = localStorage.getItem('juristicId')
      await residentService.deleteResident(deleteId.value)
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      await logService.saveLog(log.value)
    } catch (e) {
      console.log(e)
    }
    deleteDialog.value = false
    await getResidents()
    loadingStore.isLoading = false
  }

  const saveResident = async () => {
    loadingStore.isLoading = true
    try {
      console.log(editedResident.value)
      const activity = ref('')
      const role = localStorage.getItem('role')
      const id = ref('')
      if (role == '3') {
        id.value = localStorage.getItem('residentId')
      } else {
        id.value = localStorage.getItem('juristicId')
      }
      if (editedResident.value.id) {
        //edt id not empty = edt
        if (addHouseNumber.value !== '') {
          editedResident.value.house_number = addHouseNumber.value
        }
        const res = await residentService.updateResident(
          editedResident.value.id,
          editedResident.value
        )
        console.log(res)
        if (role != '3') {
          activity.value = `แก้ไขข้อมูลลูกบ้าน id: ${res.data.id}`
        } else {
          activity.value = `ลูกบ้าน id: ${res.data.id} แก้ไขข้อมูลส่วนตัว`
        }
      } else {
        //new
        const res = await residentService.saveResident(editedResident.value)
        console.log(res)
        activity.value = `เพิ่มข้อมูลลูกบ้านย้ายเข้าใหม่ id: ${res.data.id}`
      }
      const log = ref<Log>({
        activity: activity.value,
      })
      if (role != '3') {
        log.value.juristicId = parseInt(id.value)
      } else {
        log.value.residentId = parseInt(id.value)
      }
      const res = await logService.saveLog(log.value)
      console.log(res)
      
    } catch (e) {
      console.log(e)
    }
    await getResidents()
    clear()
    loadingStore.isLoading = false
  }

  const clear = () => {
    editDialog.value = false
    createDialog.value = false
    addHouseNumber.value = ''
    editedResident.value = {
      name: '',
      phone: '',
      email: '',
      house_number: ''
    }
  }

  const editResident = (resident: Resident) => {
    editDialog.value = true
    editedResident.value = { ...resident } //new object
    console.log(editedResident.value)
  }
  const selectedResident = async (id: number) => {
    const res = await residentService.getResidentByID(id)
    selectResident.value = res.data
    console.log(selectResident.value)
  }

  return {
    residents,
    deleteResident,
    editedResident,
    clear,
    saveResident,
    editResident,
    createDialog,
    editDialog,
    deleteDialog,
    getResidents,
    deleteId,
    selectedResident,
    selectResident,
    getResidentByID,
    getHouseNumberByResidentID,
    house_number,
    addHouseNumber
  }
})
