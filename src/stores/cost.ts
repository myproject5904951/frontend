import { ref } from 'vue'
import { defineStore } from 'pinia'
import costService from '@/service/cost'
import type Cost from '@/types/Cost'
// import cost from '@/service/cost'
// import { useLoadingStore } from './loading'

export const useCostStore = defineStore('cost', () => {
  const costs = ref<Cost[]>([])
  const costList = ref<String[]>([])
  // const loadingStore = useLoadingStore();

  async function getCosts() {
    // loadingStore.isLoading = true;
    try {
      const res = await costService.getCosts()
      costs.value = res.data
      costList.value = []
      for (const i of costs.value) {
        costList.value.push(i.type)
      }
      // console.log(costList.value)
    } catch (e) {
      console.log(e)
    }
    // loadingStore.isLoading = false;
  }

  return {
    getCosts,
    costs,
    costList
  }
})
