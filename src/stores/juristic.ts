import { ref } from 'vue'
import { defineStore } from 'pinia'
import jurusticService from '@/service/juristic'
import type Juristic from '@/types/Juristic'
import { useLoadingStore } from './loading'
import type Log from '@/types/Log'
import logService from '@/service/log'

export const useJuristicStore = defineStore('juristic', () => {
  const juristics = ref<Juristic[]>([])
  const createDialog = ref(false)
  const editDialog = ref(false)
  const deleteDialog = ref(false)
  const deleteId = ref(0)
  const selectJuristic = ref<Juristic>()
  const loadingStore = useLoadingStore()
  const role = ref('เจ้าหน้าที่นิติบุคคล')

  async function getJuristics() {
    loadingStore.isLoading = true
    try {
      const res = await jurusticService.getJuristics()
      juristics.value = res.data
      // console.log(juristics.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const editedJuristic = ref<Juristic>({
    name: '',
    email: '',
    address: '',
    phone: '',
  })

  const deleteJuristic = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref(`ลบข้อมูลพนักงานนิติบุคคล id: ${deleteId.value}`)
      const juristicId = localStorage.getItem('juristicId')
      const res = await jurusticService.deleteJuristic(deleteId.value)
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      await logService.saveLog(log.value)
    } catch (e) {
      console.log(e)
    }
    deleteDialog.value = false
    await getJuristics()
    loadingStore.isLoading = false
  }

  const saveJuristic = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref('')
      const juristicId = localStorage.getItem('juristicId')
      if (editedJuristic.value.id) {
        //edt id not empty = edt
        console.log(editedJuristic.value)
        const res = await jurusticService.updateJuristic(
          editedJuristic.value.id,
          editedJuristic.value
        )
        activity.value = `แก้ไขข้อมูลพนักงานนิติบุคคล id: ${res.data.id}`
      } else {
        //new
        if(role.value == 'เจ้าหน้าที่นิติบุคคล'){
          editedJuristic.value.role = 2
        }else if(role.value == 'ผู้ดูแล'){
          editedJuristic.value.role = 1
        }
        const res = await jurusticService.saveJuristic(editedJuristic.value)
        activity.value = `เพิ่มข้อมูลลูกพนักงานนิติบุคคลเข้าใหม่ id: ${res.data.juristic.id}`
      }
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      const res = await logService.saveLog(log.value)
    } catch (e) {
      console.log(e)
    }
    await getJuristics()
    clear()
    loadingStore.isLoading = false
  }

  const clear = () => {
    editDialog.value = false
    createDialog.value = false
    role.value = 'เจ้าหน้าที่นิติบุคคล'
    editedJuristic.value = {
      name: '',
      email: '',
      address: '',
      phone: '',
    }
  }

  const editJuristic = async (juristic: Juristic) => {
    editDialog.value = true
    editedJuristic.value = { ...juristic } //new object    
  }

  const selectedJuristic = async (id: number) => {
    const res = await jurusticService.getJuristicByID(id)
    selectJuristic.value = res.data
    // console.log(selectJuristic.value?.name)
  }

  const confirmDelete = async (id: number) => {
    // selectedJuristic(id);
    deleteDialog.value = true
    deleteId.value = id
  }

  return {
    juristics,
    deleteJuristic,
    editedJuristic,
    clear,
    saveJuristic,
    editJuristic,
    createDialog,
    editDialog,
    deleteDialog,
    getJuristics,
    deleteId,
    selectedJuristic,
    selectJuristic,
    confirmDelete,
    role
  }
})
