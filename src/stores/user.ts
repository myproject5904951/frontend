import { ref } from 'vue'
import { defineStore } from 'pinia'
import type User from '@/types/User'
import userService from '@/service/user'
import { useLoadingStore } from './loading'
import logService from '@/service/log'
import type Log from '@/types/Log'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])
  // const selectUser = ref<User>()
  const loadingStore = useLoadingStore()

  async function getUsers() {
    loadingStore.isLoading = true
    try {
      const res = await userService.getUsers()
      users.value = res.data
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function getUserByID(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await userService.getUserByID(id)
      editedUser.value = res.data
      editedUser.value.password = null
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const editedUser = ref<User>({
    username: '',
    password: '',
    role: 0,
    resident: null,
    juristic: null
  })

  const deleteUser = async (id: number): Promise<void> => {
    loadingStore.isLoading = true
    try {
      const res = await userService.deleteUser(id)
      console.log(res)
    } catch (e) {
      console.log(e)
    }
    await getUsers()
    loadingStore.isLoading = false
  }

  const saveUser = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref('')
      const role = localStorage.getItem('role')
      const id = ref('')
      if(role == '3'){
        id.value = localStorage.getItem('residentId')
      }else{
        id.value = localStorage.getItem('juristicId')
      }
      if (editedUser.value.id) {
        //edt id not empty = edt
        const res = await userService.updateUser(editedUser.value.id, editedUser.value)
        console.log(res)
        if (role != '3') {
          activity.value = `พนักงาน id: ${id.value} แก้ไขข้อมูลการเข้าสู่ระบบ`
        } else {
          activity.value = `ลูกบ้าน id: ${id.value} แก้ไขข้อมูลการเข้าสู่ระบบ`
        }
      } else {
        //new
        const res = await userService.saveUser(editedUser.value)
        console.log(res)
      }
      const log = ref<Log>({
        activity: activity.value,
      })
      if (role != '3') {
        log.value.juristicId = parseInt(id.value)
      } else {
        log.value.residentId = parseInt(id.value)
      }
      const res = await logService.saveLog(log.value)
      console.log(res)
    } catch (e) {
      console.log(e)
    }
    await getUsers()
    clear()
    loadingStore.isLoading = false
  }

  const clear = () => {
    editedUser.value = {
      username: '',
      password: '',
      role: 0
    }
  }

  const editUser = (user: User) => {
    editedUser.value = { ...user } //new object
    //JSON.parse(JSON.stringify(user));
  }

  return {
    users,
    deleteUser,
    editedUser,
    clear,
    saveUser,
    editUser,
    getUsers,
    getUserByID,
  }
})
