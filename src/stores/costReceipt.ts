import { ref } from 'vue'
import { defineStore } from 'pinia'
import costReceiptService from '@/service/costReceipt'
import type CostReceipt from '@/types/CostReceipt'
import logService from '@/service/log'
import { useLoadingStore } from './loading'
import type Log from '@/types/Log'
import { useCostStore } from '@/stores/cost'

export const useCostReceiptStore = defineStore('costReceipt', () => {
  const costReceipts = ref<CostReceipt[]>([])
  const costImgDialog = ref(false)
  const createCostDialog = ref(false)
  const editCostDialog = ref(false)
  const deleteDialog = ref(false)
  const loadingStore = useLoadingStore()
  const juristicId = parseInt(localStorage.getItem('juristicId')!.toString())
  const juristicName = localStorage.getItem('name')!.toString()
  const subTotalCost = ref([0])
  const totalCost = ref(0)
  const CostStore = useCostStore()

  async function getCostReceipts() {
    loadingStore.isLoading = true
    try {
      const res = await costReceiptService.getCostReceipts()
      costReceipts.value = res.data
      const cost1 = ref(0)
      const cost2 = ref(0)
      const cost3 = ref(0)
      const cost4 = ref(0)
      const cost5 = ref(0)
      const cost6 = ref(0)
      const cost7 = ref(0)
      totalCost.value = 0
      for (const cr of costReceipts.value) {
        const total = parseFloat(cr.total)
        if (cr.cost?.id == 1) {
          cost1.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 2) {
          cost2.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 3) {
          cost3.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 4) {
          cost4.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 5) {
          cost5.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 6) {
          cost6.value += total
          totalCost.value += total
        } else if (cr.cost?.id == 7) {
          cost7.value += total
          totalCost.value += total
        }
      }
      subTotalCost.value = []
      subTotalCost.value.push(cost1.value)
      subTotalCost.value.push(cost2.value)
      subTotalCost.value.push(cost3.value)
      subTotalCost.value.push(cost4.value)
      subTotalCost.value.push(cost5.value)
      subTotalCost.value.push(cost6.value)
      subTotalCost.value.push(cost7.value)

      costReceipts.value.sort(function (a, b) {
        const keyA = new Date(a.pay_date);
        const keyB = new Date(b.pay_date);
        // Compare the 2 dates
        if (keyA > keyB) return -1;
        if (keyA < keyB) return 1;
        return 0;
      })
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  const selectReceipt = ref<CostReceipt>({
    total: 0.00,
    billImg: '',
    juristicId: juristicId,
    cost: {
      type: ''
    },
    note: ''
  })

  const clear = () => {
    createCostDialog.value = false
    editCostDialog.value = false
    deleteDialog.value = false
    costImgDialog.value = false
    selectReceipt.value = {
      total: 0.0,
      billImg: '',
      juristic: {
        name: ''
      },
      juristicId: juristicId,
      cost: {
        type: ''
      },
      note: ''
    }
  }
  const showReceipt = (costReceipt: CostReceipt) => {
    selectReceipt.value = costReceipt
    costImgDialog.value = true
  }

  const saveCostReceipt = async () => {
    loadingStore.isLoading = true
    try {
      // selectReceipt.value.costId
      for (const i of CostStore.costs) {
        if (i.type === selectReceipt.value.cost.type) {
          selectReceipt.value.cost = i
          break;
        }
      }
      console.log(selectReceipt.value)
      const activity = ref('')
      const juristicId = localStorage.getItem('juristicId')
      if (selectReceipt.value.id) {
        //edt id not empty = edt
        const res = await costReceiptService.updateCostReceipt(
          selectReceipt.value.id,
          selectReceipt.value
        )
        // console.log(res)
        activity.value = `แก้ไขข้อมูลใบแจ้งค่าใช้จ่าย ${selectReceipt.value.cost?.type} id: ${res.data.id}`
      } else {
        //new
        const res = await costReceiptService.saveCostReceipt(selectReceipt.value)
        // console.log(res)
        activity.value = `เพิ่มข้อมูลใบแจ้งค่าใช้จ่าย ${selectReceipt.value.cost?.type} id: ${res.data.id}`
      }
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      const res = await logService.saveLog(log.value)
      // console.log(res)
    } catch (e) {
      console.log(e)
    }
    clear()
    await getCostReceipts()
    loadingStore.isLoading = false
  }

  const deleteCostReceipt = async () => {
    loadingStore.isLoading = true
    try {
      const activity = ref(
        `ลบข้อมูลใบแจ้งค่าใช้จ่าย ${selectReceipt.value.cost?.type} id: ${selectReceipt.value.id!}`
      )
      const juristicId = localStorage.getItem('juristicId')
      const res = await costReceiptService.deleteCostReceipt(selectReceipt.value.id!)
      console.log(res)
      const log = ref<Log>({
        activity: activity.value,
        juristicId: parseInt(juristicId!)
      })
      await logService.saveLog(log.value)
    } catch (e) {
      console.log(e)
    }
    clear()
    await getCostReceipts()
    loadingStore.isLoading = false
  }

  return {
    getCostReceipts,
    costReceipts,
    costImgDialog,
    showReceipt,
    selectReceipt,
    createCostDialog,
    clear,
    saveCostReceipt,
    editCostDialog,
    juristicName,
    deleteDialog,
    deleteCostReceipt,
    totalCost,
    subTotalCost
  }
})
