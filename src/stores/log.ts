import { ref } from 'vue'
import { defineStore } from 'pinia'
import logService from '@/service/log'
import type Log from '@/types/Log'

export const useLogStore = defineStore('log', () => {
  const logs = ref<Log[]>([])

  async function getLogs() {
    try {
      const res = await logService.getLogs()
      logs.value = res.data
    } catch (e) {
      console.log(e)
    }
  }

  return {
    getLogs, logs
  }
})
