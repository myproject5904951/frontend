import type House from './House'
import type Receipt from './Receipt'
import type User from './User'

export default interface Resident {
  id?: number
  name: string
  phone: string
  email: string
  dateOfStay?: Date
  user?: User
  house?: House[]
  house_number?: string[]
  receipt?: Receipt[]
}
