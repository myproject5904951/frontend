export default interface Juristic {
  id?: number
  name: string
  email?: string
  address?: string
  phone?: string
  role?: number
}
