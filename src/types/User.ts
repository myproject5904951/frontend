import type Juristic from './Juristic'
import type Resident from './Resident'

export default interface User {
  id?: number
  username: string
  password: string
  role: number
  juristic?: Juristic
  resident?: Resident
}
