import type Juristic from './Juristic'
import type Cost from './Cost'

export default interface CostReceipt {
  id?: number
  total: number
  pay_date?: Date
  billImg: string
  note?: string
  cost?: Cost
  juristic?: Juristic
  costId?: number
  juristicId?: number
}
