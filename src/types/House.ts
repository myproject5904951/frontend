import type Resident from './Resident'

export default interface House {
  id?: number
  house_number: string
  area?: number
  resident: Resident
}
