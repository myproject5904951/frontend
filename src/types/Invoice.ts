import type Receipt from './Receipt'
import type Resident from './Resident'

export default interface Invoice {
  id?: number
  house_number?: string
  amount: number
  start_date?: Date
  last_date?: Date
  resident?: Resident
  receipt?: Receipt | null
  year?: number
}
