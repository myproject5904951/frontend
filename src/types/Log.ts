import type Juristic from './Juristic'
import type Resident from './Resident'

export default interface Log {
  id?: number
  date?: Date
  activity?: string
  juristicId?: number
  juristic?: Juristic
  residentId?: number
  resident?: Resident
}
