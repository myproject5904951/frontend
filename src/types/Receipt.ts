import type Invoice from './Invoice'
import type Juristic from './Juristic'
import type Resident from './Resident'

export default interface Receipt {
  id?: number
  sub_total: number
  package?: string
  fines: number
  discount: number
  total: number
  paid: number
  change: number
  date?: Date
  payment_method: string
  billImg?: string
  invoiceId?: number
  invoice?: Invoice
  juristicId?: number
  juristic?: Juristic
  residentId?: number
  resident?: Resident
}
