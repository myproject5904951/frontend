import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        layout: 'MainLayout',
        title: 'Home',
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue'),
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        title: 'Login'
      }
    },
    {
      path: '/juristic',
      name: 'juristic',
      component: () => import('../views/HousingEstate/JuristicView.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Juristic',
        requiresAuth: true
      }
    },
    {
      path: '/residents',
      name: 'residents',
      component: () => import('../views/HousingEstate/ResidentView.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Residents',
        requiresAuth: true
      }
    },
    {
      path: '/resident/:id',
      name: 'resident',
      component: () => import('../views/HousingEstate/ResidentInfoView.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Resident',
        requiresAuth: true
      }
    },
    {
      path: '/resident/:id/common-fee-inv',
      name: 'commonfee-inv',
      component: () => import('../views/HousingEstate/CommonFeeInv.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Payments',
        requiresAuth: true
      }
    },
    {
      path: '/invoice-management',
      name: 'common-fee-invoice-management',
      component: () => import('../views/HousingEstate/CommonFeeManage.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Invoice',
        requiresAuth: true
      }
    },
    {
      path: '/receipt-management',
      name: 'receipt-management',
      component: () => import('../views/HousingEstate/ReceiptManage.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Receipt',
        requiresAuth: true
      }
    },
    {
      path: '/cost-receipt',
      name: 'cost-receipt',
      component: () => import('../views/HousingEstate/CostReceipt.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Cost',
        requiresAuth: true
      }
    },
    {
      path: '/account-settings/:id',
      name: 'account-settings',
      component: () => import('../views/HousingEstate/AccountSettings.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Account Settings',
        requiresAuth: true
      }
    },
    {
      path: '/log',
      name: 'log',
      component: () => import('../views/HousingEstate/LogView.vue'),
      meta: {
        layout: 'MainLayout',
        title: 'Log Activities',
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to) => {
  document.title = `MyHoue - ${to.meta.title}`
  const link = document.querySelector("[rel='icon']")
  link!.setAttribute(
    'href',
    'https://cdn.discordapp.com/attachments/746298215124697181/1129792354803527700/icon.png'
  )
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}

router.beforeEach((to) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: '/login',
      query: { redirect: to.fullPath }
    }
  }
})

export default router
