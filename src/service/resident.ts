import type Resident from '@/types/Resident'
import http from './axios'

function getResidents() {
  return http.get('/residents')
}

function getResidentByID(id: number) {
  return http.get(`/residents/${id}`)
}

function getHouseNumberByResidentID(id: number) {
  return http.get(`residents/residentId/${id}`)
}

function saveResident(resident: Resident) {
  return http.post('/residents', resident)
}

function updateResident(id: number, resident: Resident) {
  return http.patch(`/residents/${id}`, resident)
}

function deleteResident(id: number) {
  console.log('id:', id)
  return http.delete(`/residents/${id}`)
}

export default {
  getResidents,
  saveResident,
  updateResident,
  deleteResident,
  getResidentByID,
  getHouseNumberByResidentID
}
