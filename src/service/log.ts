import type Log from '@/types/Log'
import http from './axios'

function getLogs() {
  return http.get('/log')
}

function saveLog(log: Log) {
  return http.post('/log', log)
}

function updateLog(id: number, cost: Log) {
  return http.patch(`/log/${id}`, cost)
}

function deleteLog(id: number) {
  return http.delete(`/log/${id}`)
}

export default { getLogs, saveLog, updateLog, deleteLog }
