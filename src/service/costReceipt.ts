import type CostReceipt from '@/types/CostReceipt'
import http from './axios'

function getCostReceipts() {
  return http.get('/cost-receipts')
}

function getCostReceiptByID(id: number) {
  return http.get(`/cost-receipts/${id}`)
}

function saveCostReceipt(costReceipt: CostReceipt) {
  return http.post('/cost-receipts', costReceipt)
}

function updateCostReceipt(id: number, costReceipt: CostReceipt) {
  return http.patch(`/cost-receipts/${id}`, costReceipt)
}

function deleteCostReceipt(id: number) {
  return http.delete(`/cost-receipts/${id}`)
}

export default {
  getCostReceipts,
  getCostReceiptByID,
  saveCostReceipt,
  updateCostReceipt,
  deleteCostReceipt
}
