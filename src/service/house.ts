import type House from '@/types/House'
import http from './axios'

function getHouses() {
  return http.get('/houses')
}

function getHouseByID(id: number) {
  return http.get(`/houses/${id}`)
}

function saveHouse(house: House) {
  return http.post('/houses', house)
}

function updateHouse(id: number, house: House) {
  return http.patch(`/houses/${id}`, house)
}

function deleteHouse(id: number) {
  return http.delete(`/houses/${id}`)
}

export default { getHouses, getHouseByID, saveHouse, updateHouse, deleteHouse }
