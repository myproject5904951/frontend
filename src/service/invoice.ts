import type Invoice from '@/types/Invoice'
import http from './axios'

function getInvoices() {
  return http.get('/invoices')
}

function getInvoiceByID(id: number) {
  return http.get(`/invoices/${id}`)
}

function saveInvoice(invoice: Invoice) {
  return http.post('/invoices', invoice)
}

function updateInvoice(id: number, invoice: Invoice) {
  return http.patch(`/invoices/${id}`, invoice)
}

function deleteInvoice(id: number) {
  return http.delete(`/invoices/${id}`)
}

export default { getInvoices, saveInvoice, updateInvoice, deleteInvoice, getInvoiceByID }
