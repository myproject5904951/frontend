import type Cost from '@/types/Cost'
import http from './axios'

function getCosts() {
  return http.get('/costs')
}

function getCostByType(type: string) {
  return http.get(`/costs/${type}`)
}

function saveCost(cost: Cost) {
  return http.post('/costs', cost)
}

function updateCost(id: number, cost: Cost) {
  return http.patch(`/costs/${id}`, cost)
}

function deleteCost(id: number) {
  return http.delete(`/costs/${id}`)
}

export default { getCosts, getCostByType, saveCost, updateCost, deleteCost }
