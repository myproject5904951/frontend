import type Juristic from '@/types/Juristic'
import http from './axios'

function getJuristics() {
  return http.get('/juristics')
}

function getJuristicByID(id: number) {
  return http.get(`/juristics/${id}`)
}

function saveJuristic(juristic: Juristic) {
  return http.post('/juristics', juristic)
}

function updateJuristic(id: number, juristic: Juristic) {
  return http.patch(`/juristics/${id}`, juristic)
}

function deleteJuristic(id: number) {
  console.log('id:', id)
  return http.delete(`/juristics/${id}`)
}

export default { getJuristics, getJuristicByID, saveJuristic, updateJuristic, deleteJuristic }
